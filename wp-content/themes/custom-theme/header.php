<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package custom-theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header>
<nav class="navbar navbar-expand-lg navbar-light custom-header">
  <a class="navbar-brand custom-header--logo" href="#">Szkolenie-2</a>
  <button class="navbar-toggler custom-header--button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon custom-header--button--button-icon"></span>
  </button>

  <div class="collapse navbar-collapse custom-header__menu-navigation" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto custom-header__menu-navigation__menu-list">
      <li class="nav-item active custom-header__menu-navigation__custom-header__menu-list--menu-item">
        <a class="nav-link custom-header__menu-navigation__custom-header__menu-list--menu-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item custom-header__menu-navigation__custom-header__menu-list--menu-item">
        <a class="nav-link custom-header__menu-navigation__custom-header__menu-list--menu-link" href="#">Contact</a>
      </li>
      <li class="nav-item custom-header__menu-navigation__custom-header__menu-list--menu-item">
        <a class="nav-link custom-header__menu-navigation__custom-header__menu-list--menu-link" href="#">About us</a>
	  </li>
	  <li class="nav-item custom-header__menu-navigation__custom-header__menu-list--menu-item">
        <a class="nav-link custom-header__menu-navigation__custom-header__menu-list--menu-link" href="#">Product</a>
      </li>
    </ul>
  </div>
</nav>
</header>

