<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package custom-theme
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer">

	<div class="narrow my-4 mx-auto col-11">
		<div class="row mb-2">
			<div class="col-md">
				<div class="ftco-footer-widget mb-4">
					<h2 class="ftco-heading-2">Snipp.</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there
						live
						the blind texts.</p>
				</div>
			</div>
			<div class="col-md">
				<div class="ftco-footer-widget mb-4 ml-5">
					<h2 class="ftco-heading-2">Quick Links</h2>
					<ul class="list-unstyled">
						<li><a href="#" class="py-2 d-block">Home</a></li>
						<li><a href="#" class="py-2 d-block">Case studies</a></li>
						<li><a href="#" class="py-2 d-block">Services</a></li>
						<li><a href="#" class="py-2 d-block">Portfolio</a></li>
						<li><a href="#" class="py-2 d-block">About</a></li>
						<li><a href="#" class="py-2 d-block">Contact</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md">
				<div class="ftco-footer-widget mb-4">
					<h2 class="ftco-heading-2">Contact Information</h2>
					<ul class="list-unstyled">
						<li><a href="#" class="py-2 d-block">198 West 21th Street, Suite 721 New York NY 10016</a></li>
						<li><a href="#" class="py-2 d-block">+ 1235 2355 98</a></li>
						<li><a href="#" class="py-2 d-block">info@yoursite.com</a></li>
						<li><a href="#" class="py-2 d-block">email@email.com</a></li>
					</ul>
				</div>
			</div>
		</div>
		<hr class="col-11 mx-auto">
		<div class="row mx-auto col-8 social">
			<ul class="mx-auto my-0">
				<li><a href="#" class="py-1 px-3 d-block">Facebook</a></li>
				<li><a href="#" class="py-1 px-3 d-block">Instagram</a></li>
				<li><a href="#" class="py-1 px-3 d-block">LinkedIn</a></li>
			</ul>
		</div>
		<hr class="col-11 mx-auto">
		<div class="site-info mx-auto col-10">

			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'custom-theme' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'custom-theme' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
			<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'custom-theme' ), 'custom-theme', '<a href="http://underscores.me/">Underscores.me</a>' );
				?>
		</div><!-- .site-info -->
	</div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>