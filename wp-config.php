<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'szkolenie-2' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@4xY96O:F|2CO.z5!bCi3>$7!?8xA/8+v=W!%-=DQu^E1-}:^sM.<vwG:{JhOoLt' );
define( 'SECURE_AUTH_KEY',  '?ueNAbwhhHxv$onEFAHIw{:Amk}i@pow!bqGkH n*bk&1KzJ$y*W|F1UgE_i_r-h' );
define( 'LOGGED_IN_KEY',    'O2.Ox](Pm>_B5}&^Grj V?g>zBA`]A)+5sp`S$,lYddH$flDZU+ RO(f!6&]Ure+' );
define( 'NONCE_KEY',        '_yN?f?_%dpDbA_j8WZ3[@IBDj({< 0F(*bVdp{ho~kn])[{D`=,S)6ZNmGgdh(,g' );
define( 'AUTH_SALT',        '?A)eyowv1Go8gVu}_d1e01[}-1)RYc}U,VH/UPU&/kn30+T_6KGPiPT?87-fo;,f' );
define( 'SECURE_AUTH_SALT', 'HHC`g.V|d@LMpJdtPp(}Vm)JaCt/Y@o` &Q}GUqIS;/E*>[STx#qY7llk| 8WM+u' );
define( 'LOGGED_IN_SALT',   '3!7cp{tF8Z0JZLLKgZe2.t:I9C6C9wif#3^]S0JOa0oKLM)2+zrBP.h<$FBQY8EP' );
define( 'NONCE_SALT',       'mw8fJi{Z1H%%dQ7ta?fKFP6`WyKyWp]ov5HFF#65Auay#Hx?#Pyx7?pVVb}H=,7k' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
